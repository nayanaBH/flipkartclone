const express = require('express');
const path = require('path');
const cors = require('cors');

const app = express();
app.use(cors());
app.use(express.json());

const port = 3005;

const userRouter = require('./src/routes/user.routes');
const productsRouter = require('./src/routes/product.routes');

app.use("/api/users", userRouter);
app.use("/api/products", productsRouter);

app.use(express.static(path.join(__dirname, 'client/build')));
app.use("/uploads", express.static(path.join(__dirname + '/uploads')));
// Handles any requests that don't match the ones above
app.get('*', (req, res) =>{
    res.sendFile(path.join(__dirname+'/client/build/index.html'));
});

app.listen(port, () => {
    console.log("Server is up on port: ", port);
})