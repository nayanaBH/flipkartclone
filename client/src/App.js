import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import { ConnectedRouter } from 'connected-react-router';
import { Switch, Route } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';
import HomeContainer from './components/Home/HomeContainer';
import Header from './components/Common/Header';
import Footer from './components/Common/Footer';
import Login from './components/Auth/Login';
import authActions from './components/Auth/redux/actions';
import ProductContainer from './components/Product/ProductContainer';
import CartContainer from './components/Cart/CartContainer';
import ProductDetailContainer from './components/Product/ProductDetailContainer';

class App extends Component {

  componentDidMount() {
    this.props.checkIsAuthenticated(this.props.history);
  }

  navigate = route => this.props.history.push(route);

  render() {
    return (
      <div>
        <Header 
          isAuthenticated={this.props.isAuthenticated} 
          userInfo={this.props.userInfo}
          cartData={this.props.cartData}
          navigate={this.navigate}
          logoutUser={this.props.logoutUser} />
        <ConnectedRouter history={this.props.history}>
          <Switch>
            <Route exact path="/" component={HomeContainer} />
            <Route exact path="/home" component={HomeContainer} />
            <Route exact path="/products" component={ProductContainer} />
            <Route exact path="/product-detail" component={ProductDetailContainer} />
            <Route exact path="/cart" component={CartContainer} />
            <Route path="/login" component={Login} />
          </Switch>
        </ConnectedRouter>
        <ToastContainer 
          position="top-right"
          autoClose={3000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          pauseOnHover /> <br /><br /> 
          <Footer />
      </div> 

    )
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  userInfo: state.auth.userInfo,
  cartData: state.cart.cartData,
})

const mapDispatchToProps = dispatch => ({
  checkIsAuthenticated: (value) => dispatch(authActions.checkIsAuthenticated(value)),
  logoutUser: () => dispatch(authActions.logoutUser()),
})

export default connect(mapStateToProps, mapDispatchToProps)(App);
