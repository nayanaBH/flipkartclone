import axios from 'axios';
import { toast } from 'react-toastify';
import { clear, load } from './localStorage';

const http = axios.create();

http.interceptors.response.use((res) => {
    return res;
}, (error) => {
    console.log('error', error.response)
    if (error && error.response && error.response.status === 401) {
        toast.error("Session expired. Please login again to continue");
        clear();
        setTimeout(() => {
            window.location.reload();
        }, 2000);
    }
    return Promise.reject(error);
});

http.interceptors.request.use((config) => {
    config.headers.authorization = 'Bearer ' + load('token');
    return config;
}, (error) => {
    return Promise.reject(error);
});

export default http;