import http from './interceptor';
let baseURL;
if (process.env.NODE_ENV === 'development') {
    baseURL = 'http://localhost:3005';
} else {
    baseURL = window.location.origin;
}

export const loginUser = async (req) => {
    const url = `${baseURL}/api/users/login`;
    return new Promise((resolve, reject) => {
        http.post(url, req)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
    })
}

export const getProducts = async (req) => {
    const url = `${baseURL}/api/products`;
    return new Promise((resolve, reject) => {
        http.get(url, req)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
    })
}

