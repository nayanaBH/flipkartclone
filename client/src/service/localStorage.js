export const save = (key, value) => {
  localStorage.setItem(key, JSON.stringify(value))
}

export const load = key => {
  try {
    const value = localStorage.getItem(key);
    return value ? JSON.parse(value) : null;
  } catch (error) {
    localStorage.clear();
    window.location.reload();
  }
}

export const clear = () => localStorage.clear();