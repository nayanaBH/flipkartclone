export const getAuthState = (state) => state.auth;
export const getCartState = (state) => state.cart;
export const getProductState = (state) => state.product;