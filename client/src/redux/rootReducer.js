import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import homeReducer from '../components/Home/redux/reducer';
import authReducer from '../components/Auth/redux/reducer';
import productReducer from '../components/Product/redux/reducer';
import cartReducer from '../components/Cart/redux/reducer';

const rootReducer = history => combineReducers({
    router: connectRouter(history),
    home: homeReducer,
    auth: authReducer,
    product: productReducer,
    cart: cartReducer
})

export default rootReducer;