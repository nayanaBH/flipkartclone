import { all } from 'redux-saga/effects';

import { homeSaga } from '../components/Home/redux/operations';
import { authSaga } from '../components/Auth/redux/operations';
import { productSaga } from '../components/Product/redux/operations';
import { cartSaga } from '../components/Cart/redux/operations';

export default function* rootSaga() {
    yield all([
        ...homeSaga,
        ...authSaga,
        ...productSaga,
        ...cartSaga
    ])
}