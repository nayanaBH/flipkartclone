import React from 'react';
import ReactDOM from 'react-dom';
import './bootstrap.min.css';
import './index.css';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';

import createRootReducer from './redux/rootReducer';
import rootSaga from './redux/rootSaga';

import App from './App';
import reportWebVitals from './reportWebVitals';

const history = createBrowserHistory({ basename: '/' });
const sagaMiddleware = createSagaMiddleware();

let reducer = createRootReducer(history);
let store = createStore(
  reducer,
  applyMiddleware(sagaMiddleware, routerMiddleware(history))
)

sagaMiddleware.run(rootSaga);

ReactDOM.render(
  <Provider store={store}>
    <App history={history} />
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
