import React, { Component } from 'react'
import Constants from '../Common/Constants';

export default class HomeContainer extends Component {

    navigate = route => this.props.history.push(route);

    render() {
        return (
            <div className="container">
                <div className="jumbotron">
                    <h1 className="display-3">Welcome to Flipcart Clone</h1>
                    <br />
                    <p className="lead">
                        <a className="btn btn-primary btn-lg" 
                            role="button" 
                            onClick={() => this.navigate(Constants.Routes.PRODUCTS)}>
                            Browse All Products
                        </a>
                    </p>
                </div>
            </div>
        )
    }
}
