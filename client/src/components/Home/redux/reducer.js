import types from './types';

const INITIAL_STATE = {
    recentArticles: []
}

const homeReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case types.SET_RECENT_ARTICLES:
            let newState = {
                ...state,
                recentArticles: [ ...action.value ]
            }
            return newState;
    
        default:
            return state;
    }
}

export default homeReducer;