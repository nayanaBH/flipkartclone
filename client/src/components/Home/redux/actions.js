import types from './types';

export const getRecentArticles = value => ({ type: types.GET_RECENT_ARTICLES, value });

export const setRecentArticles = value => ({ type: types.SET_RECENT_ARTICLES, value });

export default {
    getRecentArticles,
    setRecentArticles
}