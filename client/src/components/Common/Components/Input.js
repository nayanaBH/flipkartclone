import React from 'react';
import classnames from 'classnames';

export default function Input({
    inputField,
    inputLabel,
    inputChange,
    inputName,
    inputType
}) {
    if (!inputField) {
        return null;
    }
    return (
        <div className={classnames('form-group', {
            'has-danger': inputField.touched && !inputField.valid
        })}>
            <label>{ inputLabel }</label>
            <input 
                type={inputType} 
                className={classnames('form-control', {
                    'is-invalid': inputField.touched && !inputField.valid
                })}
                name={inputName}
                value={inputField.value}
                onChange={(event) => inputChange(event)} 
            />
            <div className="error-text pt-1">
                { inputField.errorMessage }
            </div>
        </div>
    )
}
