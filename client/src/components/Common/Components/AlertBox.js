import React from 'react'

export const AlertBox = ({ errorMessage }) => (<div className="alert alert-danger">
    <button type="button" className="close">&times;</button>
    { errorMessage }
</div>);
