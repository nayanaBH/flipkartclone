import React from 'react';
import Constants from './Constants';

const Header = ({ isAuthenticated, userInfo, cartData, navigate, logoutUser }) => {
    return ( 
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
            <div className="container">
                <a className="navbar-brand" onClick={() => navigate(Constants.Routes.HOME)}>Flipkart Clone</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarColor01">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item active">
                            <a className="nav-link c-pointer" onClick={() => navigate(Constants.Routes.HOME)}>Home
                                <span className="sr-only">(current)</span>
                            </a>
                        </li>
                        <li className="nav-item active">
                            <a className="nav-link c-pointer" onClick={() => navigate(Constants.Routes.PRODUCTS)}>Products</a>
                        </li>
                        {
                            isAuthenticated
                            ? <>
                                <li className="nav-item active">
                                    <a className="nav-link c-pointer" onClick={logoutUser}>Logout</a>
                                </li>
                                <li className="nav-item active">
                                    <a className="nav-link c-pointer" onClick={() => navigate(Constants.Routes.CART)}>
                                        <i className="fa fa-shopping-cart">
                                            <span>{cartData && cartData.length}</span>
                                        </i> 
                                    </a>
                                </li>
                            </>
                            : <li className="nav-item active">
                                <a className="nav-link c-pointer" onClick={() => navigate(Constants.Routes.LOGIN)}>Login</a>
                            </li>
                        }
                    </ul>
                </div>

            </div>
        </nav>
    )
}

export default Header;
