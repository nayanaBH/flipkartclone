const Constants = {
    Routes: {
        ROOT: '/',
        LOGIN: '/login',
        REGISTER: '/register',
        HOME: '/home',
        PRODUCTS: '/products',
        PRODUCT_DETAIL: '/product-detail',
        CART: '/cart'
    },
    Login: {
        ERROR: 'Something went wrong. Please try again later.'
    }
}

export default Constants;