import React from 'react';

const ProductSearch = ({ searchChangeHandler }) => {
    return (
        <div className="mt-4">
            <input type="search" className="form-control" placeholder="Search product..." onChange={(e) => searchChangeHandler(e)} />
        </div>
    )
}

export default ProductSearch;
