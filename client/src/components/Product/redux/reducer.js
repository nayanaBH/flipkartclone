import types from './types';

const INITIAL_STATE = {
    products: [],
    productDetail: {},
    allProducts: []
}

const productReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case types.SET_PRODUCTS:
            return {
                ...state,
                products: [ ...action.value ]
            };
        case types.SET_ALL_PRODUCTS:
            return {
                ...state,
                allProducts: [ ...action.value ]
            };
        case types.SET_PRODUCT_DETAIL:
            return {
                ...state,
                productDetail: action.value
            };
    
        default:
            return state;
    }
}

export default productReducer;