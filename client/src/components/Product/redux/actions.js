import types from './types';

export const getProducts = value => ({ type: types.GET_PRODUCTS, value });

export const setAllProducts = value => ({ type: types.SET_ALL_PRODUCTS, value });

export const setProducts = value => ({ type: types.SET_PRODUCTS, value });

export const setProductDetail = value => ({ type: types.SET_PRODUCT_DETAIL, value });

export const searchProduct = value => ({ type: types.SEARCH_PRODUCT, value });

export default {
    getProducts,
    setProducts,
    setAllProducts,
    setProductDetail,
    searchProduct
}