import { toast } from 'react-toastify';
// import { showLoading, hideLoading } from 'react-redux-loading-bar';
import * as services from '../../../service/services';
import { getProductState } from '../../../redux/selector';
import actions from './actions';
import types from './types';
import { put, takeLatest, call, select } from 'redux-saga/effects';

function* getProducts(action) {
    try {
        const req = action.value;
        const response = yield call(services.getProducts, req);
        if (response && Object.keys(response).length > 0
            && response.data && Object.keys(response.data).length > 0) {
            yield put(actions.setProducts(response.data));
            yield put(actions.setAllProducts(JSON.parse(JSON.stringify(response.data))));
        }
    } catch (error) {
        console.log('error', error)
        toast.error("Error occurred while fetching products.");
    }
}

function* searchProduct(action) {
    try {
        let searchString = action.value;
        const productState = yield select(getProductState);
        let { allProducts } = productState;
        allProducts = allProducts.filter(product => product.Name.toLowerCase().indexOf(searchString) > -1);
        yield put(actions.setProducts(allProducts));
    } catch (error) {
        console.log('error', error)
        toast.error("Error occurred while fetching products.");
    }
}

export const productSaga = [
    takeLatest(types.GET_PRODUCTS, getProducts),
    takeLatest(types.SEARCH_PRODUCT, searchProduct)
]