import React, { Component } from 'react';
import { connect } from 'react-redux';
import ProductSearch from './ProductSearch';
import actions from './redux/actions';
import cartActions from '../Cart/redux/actions';

class ProductDetailContainer extends Component {

    
    addToCartHandler = product => {
        this.props.addProductInCart(product);
    }


    render() {
        const { productDetail } = this.props;
        return (
            <div className="container">
                <div className="row mt-4">
                {
                    productDetail && Object.keys(productDetail).length > 0
                    ? <div className="col-md-10" key={productDetail.Product_ID}>
                            <div className="card mb-3">
                            <p className="card-header">
                                { productDetail.Name }
                            </p>
                            <img className="card-img-top c-poimter" src={productDetail.Product_URL} />
                            <div className="card-body">
                                <p className="card-text">Price: <i className="fa fa-inr"></i> {productDetail.Price}</p> 
                                <p className="card-text">Description: {productDetail.Description}</p>
                                <a className="btn btn-primary" onClick={() => this.addToCartHandler(productDetail)}>Add to Cart</a>
                            </div>
                        </div>
                    </div>
                    : null
                }
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    productDetail: state.product.productDetail,
})

const mapDispatchToProps = dispatch => ({
    addProductInCart: (value) => dispatch(cartActions.addProductInCart(value))
})

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetailContainer);
