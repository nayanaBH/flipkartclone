import React, { Component } from 'react';
import { connect } from 'react-redux';
import ProductSearch from './ProductSearch';
import actions from './redux/actions';
import cartActions from '../Cart/redux/actions';
import Constants from '../Common/Constants';

class ProductContainer extends Component {

    constructor(props) {
        super(props)
        this.state = {
             searchString: ''
        }
    }

    componentDidMount() {
        this.props.getProducts({});
    }

    addToCartHandler = product => this.props.addProductInCart(product);

    navigate = (route, product) => {
        this.props.setProductDetail(product);
        this.props.history.push(route);
    };

    searchChangeHandler = e => {
        this.setState({ searchString: e.target.value });
        this.props.searchProduct(e.target.value);
    }

    render() {

        const { products } = this.props;
        
        return (
            <div className="container">
                <ProductSearch searchChangeHandler={this.searchChangeHandler} />
                <div className="row mt-4">
                {
                    products && products.length
                    ? products.map(product => {
                        return (<div className="col-md-3" key={product.Product_ID}>
                            <div className="card mb-3">
                            <p className="card-header">
                                { product.Name }
                            </p>
                            <img style={{height: '150px'}} className="card-img-top" src={product.Product_URL} onClick={() => this.navigate(Constants.Routes.PRODUCT_DETAIL, product)} />
                            <div className="card-body">
                                <p className="card-text">Price: <i className="fa fa-inr"></i> {product.Price}</p>
                                <a className="btn btn-primary" onClick={() => this.addToCartHandler(product)}>Add to Cart</a>
                            </div>
                        </div>
                        </div>)
                    }) : <p className="lead text-center">No records found.</p>
                }
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    products: state.product.products,
})

const mapDispatchToProps = dispatch => ({
    getProducts: (value) => dispatch(actions.getProducts(value)),
    setProductDetail: (value) => dispatch(actions.setProductDetail(value)),
    searchProduct: (value) => dispatch(actions.searchProduct(value)),
    addProductInCart: (value) => dispatch(cartActions.addProductInCart(value)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ProductContainer);
