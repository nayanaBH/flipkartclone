import types from './types';

export const addProductInCart = value => ({ type: types.ADD_PRODUCT_IN_CART, value });

export const setProductsInCart = value => ({ type: types.SET_PRODUCTS_IN_CART, value });

export const removeProductFromCart = value => ({ type: types.REMOVE_PRODUCT_FROM_CART, value });

export const setAddedCartProduct = value => ({ type: types.SET_ADDED_CART_PRODUCT, value });

export default {
    addProductInCart,
    removeProductFromCart,
    setProductsInCart,
    setAddedCartProduct
}