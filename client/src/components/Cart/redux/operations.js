import { toast } from 'react-toastify';
import { push } from 'react-router-redux';
import * as services from '../../../service/services';
import actions from './actions';
import types from './types';
import { put, takeLatest, call, select } from 'redux-saga/effects';
import { getCartState, getAuthState } from '../../../redux/selector';
import Constants from '../../Common/Constants';

function* addProductInCart(action) {
    try {
        const product = action.value;
        const cartState = yield select(getCartState);
        const authState = yield select(getAuthState);
        const { isAuthenticated } = authState;
        const { cartData } = cartState;
        let isProductExists = false;
        if (isAuthenticated) {
            if (cartData && cartData.length) {
                 cartData.forEach(cart => {
                    if (product.Product_ID === cart.Product_ID) {
                        cart.quantity++;
                        isProductExists = true;
                    }
                });
                if (!isProductExists) {
                    cartData.push({ ...product, quantity: 1 });
                }
            } else {
                cartData.push({ ...product, quantity: 1 });
            }
            toast.success(`Product ${product.Name} added successfully in the cart.`);
            yield put(actions.setProductsInCart(cartData));
            yield put(actions.setAddedCartProduct({}));
        } else {
            toast.warning("Please login to continue shopping.");
            yield put(actions.setAddedCartProduct({ ...product, quantity: 1 }));
            yield put(push(Constants.Routes.LOGIN));
        }
    } catch (error) {
        toast.error("Error occurred while adding product in the cart.");
    }
}

function* removeProductFromCart(action) {
    try {
        let id = action.value;
        const cartState = yield select(getCartState);
        let { cartData } = cartState;
        if (cartData && cartData.length) {
            cartData = cartData.filter(cart => cart.Product_ID !== id);
            toast.success(`Product removed from the cart successfully.`);
            yield put(actions.setProductsInCart(cartData));
        }
    } catch (error) {
        toast.error("Error occurred while removing product from cart.");
    }
}

export const cartSaga = [
    takeLatest(types.ADD_PRODUCT_IN_CART, addProductInCart),
    takeLatest(types.REMOVE_PRODUCT_FROM_CART, removeProductFromCart),
]