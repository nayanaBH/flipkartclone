import types from './types';

const INITIAL_STATE = {
    cartData: [],
    addedCartProduct: {}
}

const cartReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case types.SET_PRODUCTS_IN_CART:
            return {
                ...state,
                cartData: [ ...action.value ]
            };
        case types.SET_ADDED_CART_PRODUCT:
            return {
                ...state,
                addedCartProduct: action.value
            };
    
        default:
            return state;
    }
}

export default cartReducer;