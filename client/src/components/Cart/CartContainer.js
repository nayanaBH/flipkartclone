import React, { Component } from 'react';
import { connect } from 'react-redux';
import actions from './redux/actions';

class CartContainer extends Component {

    getCartTotal = cartData => {
        let total = 0;
        if (cartData && cartData.length) {
            cartData.forEach(cart => {
                total = total + (cart.Price * cart.quantity)
            });
        }
        return total;
    }

    removeClickHandler = id => {
        this.props.removeProductFromCart(id);
    }

    render() {
        const { cartData } = this.props;
        return (
            <div className="container">
                <table className="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Remove</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        cartData && cartData.length
                        ? cartData.map(cart => {
                            return (<tr key={cart.Product_ID}>
                                <td>
                                    <img src={cart.Product_URL} style={{ height: '50px'}} />
                                </td>
                                <td>{ cart.Name }</td>
                                <td><i className="fa fa-inr"></i> { cart.Price * cart.quantity }</td>
                                <td>{ cart.quantity }</td>
                                <td>
                                    <button className="btn btn-sm btn-danger" title="Delete" onClick={() => this.removeClickHandler(cart.Product_ID)}>
                                        <i className="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>)
                        }) : null
                    }
                    {
                        cartData && cartData.length
                        ? <tr>
                            <td><b>Total</b></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><b><i className="fa fa-inr"></i> { this.getCartTotal(cartData) }</b></td>
                        </tr> : null
                    }
                    </tbody>
                </table>
                {
                    cartData && cartData.length === 0
                    ? <p className="lead text-center">Cart is empty.</p> : null
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    cartData: state.cart.cartData
})

const mapDispatchToProps = dispatch => ({
    removeProductFromCart: (value) => dispatch(actions.removeProductFromCart(value)),
    addProductInCart: (value) => dispatch(actions.addProductInCart(value))
})

export default connect(mapStateToProps, mapDispatchToProps)(CartContainer);
