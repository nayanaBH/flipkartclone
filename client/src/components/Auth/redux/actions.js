import types from './types';

export const loginUser = value => ({
    type: types.LOGIN_USER,
    value: value
});

export const loginInputChange = value => ({
    type: types.LOGIN_INPUT_CHANGE,
    value: value
});

export const setLoginError = value => ({
    type: types.SET_LOGIN_ERROR,
    value: value
});

export const setLoginLoading = value => ({
    type: types.SET_IS_LOGIN_LOADING,
    value: value
});

export const setLoginInput = value => ({
    type: types.SET_LOGIN_INPUT,
    value: value
});

export const setIsAuthenticated = value => ({
    type: types.SET_IS_AUTHENTICATED,
    value: value
});

export const checkIsAuthenticated = value => ({
    type: types.CHECK_IS_AUTHENTICATED,
    value: value
});

export const logoutUser = value => ({
    type: types.LOGOUT_USER,
    value: value
});

export const setUserInfo = value => ({
    type: types.SET_USER_INFO,
    value: value
});

export const registerUser = value => ({
    type: types.REGISTER_USER,
    value: value
});

export const registerInputChange = value => ({
    type: types.REGISTER_INPUT_CHANGE,
    value: value
});

export const setRegisterError = value => ({
    type: types.SET_REGISTER_ERROR,
    value: value
});

export const setIsRegisterLoading = value => ({
    type: types.SET_IS_REGISTER_LOADING,
    value: value
});

export const setRegisterInput = value => ({
    type: types.SET_REGISTER_INPUT,
    value: value
});

export default {
    loginUser,
    loginInputChange,
    setLoginError,
    setLoginLoading,
    setLoginInput,
    setIsAuthenticated,
    checkIsAuthenticated,
    logoutUser,
    setUserInfo,

    registerUser,
    registerInputChange,
    setRegisterError,
    setIsRegisterLoading,
    setRegisterInput
}