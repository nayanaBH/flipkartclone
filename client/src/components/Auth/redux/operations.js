import { push } from 'react-router-redux';
import isEmail from 'validator/lib/isEmail';
import { put, takeLatest, select, delay, call } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import actions from './actions';
import cartActions from '../../Cart/redux/actions';
import types from './types';
import { getAuthState, getCartState } from '../../../redux/selector';
import { validateEmpty } from '../../../utils';
import Constants from '../../Common/Constants';
import * as services from '../../../service/services';
import * as localStorageService from '../../../service/localStorage';

function* loginInputChange(action) {
    const auth = yield select(getAuthState);
    const { formControls } = auth;
    const event = action.value;
    const name = event.target.name;
    const value = event.target.value;
    formControls[name].value = value;
    formControls[name].touched = true;
    formControls[name].valid = validateEmpty(value);
    if (!formControls[name].valid) {
        formControls[name].errorMessage = name + ' is required.';
    } else {
        formControls[name].errorMessage = '';
    }
    yield put(actions.setLoginInput(formControls));
}

function* loginUser(action) {
    const auth = yield select(getAuthState);
    const cartState = yield select(getCartState);
    const { addedCartProduct } = cartState;
    const { formControls } = auth;
    const { email, password } = formControls;
    email.touched = true;
    email.valid = validateEmpty(email.value);
    password.touched = true;
    password.valid = validateEmpty(password.value);
    if (!email.valid) {
        email.errorMessage = 'Email is required.';
    } else {
        email.errorMessage = '';
    }
    if (email.valid) {
        email.valid = isEmail(email.value);
        if (!email.valid) {
            email.errorMessage = 'Please enter valid email.';
        } else {
            email.errorMessage = '';
        }
    }
    if (!password.valid) {
        password.errorMessage = 'Password is required.';
    } else {
        password.errorMessage = '';
    }
    yield put(actions.setLoginInput(formControls));
    if (email.valid && password.valid) {
        try {
            const response = yield call(services.loginUser, {
                email: email.value,
                password: password.value
            });
            if (response && response.data) {
                localStorageService.save('token', response.data.token);
                localStorageService.save('user', response.data.user);
                yield put(actions.setUserInfo(response.data.user));
                yield put(actions.setIsAuthenticated(true));
                yield put(push(Constants.Routes.HOME));
                if (addedCartProduct && Object.keys(addedCartProduct).length > 0) {
                    yield put(cartActions.addProductInCart(addedCartProduct));
                }
            }
        } catch (error) {
            const response = error.response;
            let message = Constants.Login.ERROR;
            if (response && Object.keys(response).length > 0) {
                if (response.data && Object.keys(response.data).length > 0) {
                    message = response.data.message;
                }
            }
            toast.error(message);
        }
    }
}

function* checkIsAuthenticated(action) {
    const history = action.value;
    const pathname = history.location.pathname;
    try {
        if (localStorageService.load('token')) {
            yield put(actions.setIsAuthenticated(true));
            if (pathname === Constants.Routes.LOGIN) {
                yield put(push(Constants.Routes.HOME));
            }
        } else {
            yield put(actions.setIsAuthenticated(false));
        }
    } catch (error) {
        yield put(actions.logoutUser());
    }
}

function* logoutUser() {
    localStorageService.clear();
    yield put(push(Constants.Routes.HOME));
    yield put(actions.setIsAuthenticated(false));
    yield delay(1000);
    window.location.reload();
}

export const authSaga = [
    takeLatest(types.LOGIN_INPUT_CHANGE, loginInputChange),
    takeLatest(types.LOGIN_USER, loginUser),
    takeLatest(types.CHECK_IS_AUTHENTICATED, checkIsAuthenticated),
    takeLatest(types.LOGOUT_USER, logoutUser)
]