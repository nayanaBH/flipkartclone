import types from './types';

const INITIAL_STATE = {
    formControls: {
        email: {
            value: '',
            touched: false,
            valid: false,
            errorMessage: ''
        },
        password: {
            value: '',
            touched: false,
            valid: false,
            errorMessage: ''
        }
    },
    formControlsRegister: {
        firstName: {
            value: '',
            touched: false,
            valid: false,
            errorMessage: ''
        },
        lastName: {
            value: '',
            touched: false,
            valid: false,
            errorMessage: ''
        },
        email: {
            value: '',
            touched: false,
            valid: false,
            errorMessage: ''
        },
        password: {
            value: '',
            touched: false,
            valid: false,
            errorMessage: ''
        }
    },
    loginError: '',
    isLoginLoading: false,
    isAuthenticated: false,
    userInfo: {}
}

const authReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case types.SET_LOGIN_INPUT:
            return {
                ...state,
                formControls: { ...action.value }
            }
        case types.SET_LOGIN_ERROR:
            return {
                ...state,
                loginError: action.value
            }
        case types.SET_IS_LOGIN_LOADING:
            return {
                ...state,
                isLoginLoading: action.value
            }
        case types.SET_IS_AUTHENTICATED:
            return {
                ...state,
                isAuthenticated: action.value
            }
        case types.SET_USER_INFO:
            return {
                ...state,
                userInfo: { ...action.value }
            }
    
        default:
            return state;
    }
}

export default authReducer;