import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import actions from './redux/actions';
import Input from '../Common/Components/Input';
import { AlertBox } from '../Common/Components/AlertBox';
import Constants from '../Common/Constants';
import './Login.css';

class Login extends Component {

    navigate = route => this.props.history.push(route);

    submit = (event) => {
        event.preventDefault();
        this.props.loginUser({});
    }

    render() {

        const { loginError, isLoginLoading, formControls, loginInputChange } = this.props;
        const { email, password } = formControls;

        return (
            <div className="container login">
                <div className="row justify-content-center mt-4">
                    <div className="col-md-6">
                        <div className="card">
                            <div className="card-header">Login</div>
                            <div className="card-body">
                                {loginError && <AlertBox loginError={loginError} />}
                                <form onSubmit={this.submit}>
                                    <Input
                                        inputField={email}
                                        inputLabel="Email"
                                        inputChange={loginInputChange}
                                        inputName="email"
                                        inputType="text"
                                    />
                                    <Input
                                        inputField={password}
                                        inputLabel="Password"
                                        inputChange={loginInputChange}
                                        inputName="password"
                                        inputType="password"
                                    />
                                    <div className="form-group text-center">
                                        <button
                                            type="submit"
                                            disabled={isLoginLoading}
                                            className="btn btn-success primary">
                                            {isLoginLoading ? 'Please Wait...' : 'Login'}
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    loginError: state.auth.loginError,
    isLoginLoading: state.auth.isLoginLoading,
    formControls: state.auth.formControls
})

const mapDispatchToProps = dispatch => ({
    loginInputChange: (value) => dispatch(actions.loginInputChange(value)),
    loginUser: (value) => dispatch(actions.loginUser(value))
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
